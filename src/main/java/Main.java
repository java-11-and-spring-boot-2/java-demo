import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpRequest;
import java.time.Duration;
import java.util.stream.Stream;

public class Main {

    public static void main(String[] args) throws URISyntaxException {
        System.out.println("Hello World");
        try (var closeable = new CloseableResource()) {
            closeable.accessResource();
        } catch (Exception ex) {

        }
        var stream = Stream.iterate("a", (n) -> n +="a");
        var reduced = Stream.of(1,2,3).reduce((n1, n2) -> n2+n1);
        stream.limit(20).forEach(System.out::println);
        System.out.println(reduced.get());

        HttpRequest request = HttpRequest.newBuilder()
                .uri(new URI("https://postman-echo.com/get"))
                .header("key", "value")
                .timeout(Duration.ofSeconds(30))
                .GET()
                .build();
    }
}
