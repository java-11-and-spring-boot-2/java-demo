public class CloseableResource implements AutoCloseable {

    public void accessResource() {
        System.out.println("accesed resource");
    }

    @Override
    public void close() throws Exception {
        System.out.println("Closed Resource");
    }
}
